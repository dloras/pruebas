package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class ClienteTests {

	static GestorContabilidad gestor;
	static Cliente cliente1;
	static Cliente cliente2;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gestor = new GestorContabilidad();
		cliente1 = new Cliente("David", "123456", LocalDate.now());
		cliente2 = new Cliente ("Fernando", "112233", LocalDate.now());
	}
	
	@Before
	public void prepararTest() {
		gestor.listaClientes.clear();
	}
	/*
	 * Test que comprueba el metodo buscarCliente(), 
	 * buscando un cliente inexistente en una lista sin clientes,
	 * por lo que nos tendra que devolver null para que este correcto.
	 */
	@Test
	public void testBuscarClienteInexistente() {
		String dni = "123456";
		Cliente actual = gestor.buscarCliente(dni);
		assertNull(actual);
	}
	/*
	 * Test que comprueba el metodo buscarCliente(),  
	 * en este caso, con un cliente existente, siendo
	 * el cliente que nos devuelva el metodo.
	 */
	@Test
	public void testBuscarClienteExistente() {
		gestor.listaClientes.add(cliente1);
		
		Cliente actual = gestor.buscarCliente("123456");
		assertSame(cliente1, actual);
	}
	/*
	 * Test que comprueba el metodo buscarCliente()
	 * para un cliente inexistente habiendo clientes.
	 * Para que pase con exito nos devolvera null.
	 */
	@Test
	public void testBuscarClienteInexistenteHabiendoClientes() {
		gestor.listaClientes.add(cliente1);
		
		String dni = "112233";
		Cliente actual = gestor.buscarCliente(dni);
		assertNull(actual);
	}
	/*
	 * Test que comprueba el metodo buscarCliente()
	 * para un cliente existente habiendo varios clientes.
	 * El metodo nos devolvera ese cliente existente.
	 */
	@Test
	public void testBuscarClienteExistenteConVariosClientes() {
		gestor.listaClientes.add(cliente1);
		gestor.listaClientes.add(cliente2);

		Cliente actual = gestor.buscarCliente("112233");
		assertSame(cliente2, actual);
	}
	/*
	 * Test que comprueba el metodo altaCliente(), 
	 * creando un cliente inexistente,
	 * por lo cual el tama�o de listaClientes sera 1.
	 */
	@Test
	public void testAltaClienteInexistente() {
		gestor.altaCliente(cliente1);
		
		int actual = gestor.listaClientes.size();
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comprueba el metodo altaCliente()
	 * con un cliente nuevo ya existente,
	 * en este caso la listaClientes mantiene su tama�o.
	 */
	@Test
	public void testAltaClienteExistente() {
		gestor.listaClientes.add(cliente1);
		gestor.altaCliente(cliente1);
		
		int actual = gestor.listaClientes.size();
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comprueba el metodo eliminarCliente(),
	 * borrando un cliente inexistente.
	 * Como resultado, el tama�o del arraylist se mantiene igual. 
	 */
	@Test
	public void testEliminarClienteInexistente() {
		gestor.listaClientes.add(cliente1);
		gestor.eliminarCliente("XXXXXX");
		
		int actual = gestor.listaClientes.size();
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comprueba el metodo eliminarCliente(),
	 * en este caso, metemos dos clientes y al borrar
	 * un cliente de estos el arraylist pasa de 2 a 1.
	 */
	@Test
	public void testEliminarClienteExistente() {
		gestor.listaClientes.add(cliente1);
		gestor.listaClientes.add(cliente2);
		gestor.eliminarCliente("123456");
		
		int actual = gestor.listaClientes.size();
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comprueba el metodo clienteMasAntiguo(),
	 * con un unico cliente existente, que es el mismo que nos devuelve.
	 */
	@Test
	public void testClienteMasAntiguoConUnCliente() {
		cliente1.setFechaAlta(LocalDate.now().minusYears(10));
		gestor.listaClientes.add(cliente1);
		
		Cliente actual = gestor.clienteMasAntiguo();
		assertEquals(cliente1, actual);
	}
	/*
	 * Test que comprueba el metodo clienteMasAntiguo(),
	 * igual que el anterior, pero con varios clientes.
	 */
	@Test
	public void testClienteMasAntiguoConVariosClientes() {
		cliente1.setFechaAlta(LocalDate.now().minusYears(10));
		gestor.listaClientes.add(cliente1);
		gestor.listaClientes.add(cliente2);
		
		Cliente actual = gestor.clienteMasAntiguo();
		assertEquals(cliente1, actual);
	}
	/*
	 * Test que comprueba el metodo clienteMasAntiguo(),
	 * sin clientes en la listaClientes, por lo que para que pase 
	 * el test nos tiene que devolver null.
	 */
	@Test
	public void testClienteMasAntiguoSinClientes() {
		Cliente actual = gestor.clienteMasAntiguo();
		assertNull(actual);
	}

}

package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class FacturaTests {
	
	static GestorContabilidad gestor;
	static Cliente cliente1;
	static Cliente cliente2;
	static Factura factura1;
	static Factura factura2;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gestor = new GestorContabilidad();
		cliente1 = new Cliente ("David", "123456", LocalDate.now());
		factura1 = new Factura("AA11", LocalDate.now(), "Videojuego", 2.5F, 3, cliente1);
		cliente2 = new Cliente ("Fernando", "11223344", LocalDate.now());
		factura2 = new Factura("BBBB", LocalDate.now(), "Pelicula", 20, 2, cliente2);
	}
	
	@Before
	public void prepararTest() {
		gestor.listaClientes.clear();
		gestor.listaFacturas.clear();
	}
	/*
	 * Test que comprueba el metodo buscarFactura(),
	 * con una factura inexistente, devolviendo null.
	 */
	@Test
	public void testBuscarFacturaInexistente() {
		String codigoFactura = "AA11";
		Factura actual = gestor.buscarFactura(codigoFactura);
		assertNull(actual);
	}
	/*
	 * Test que comprueba el metodo buscarFactura(),
	 * con una factura existente, que es la que nos devuelve.
	 */
	@Test
	public void testBuscarFacturaExistente() {
		gestor.listaFacturas.add(factura1);
		
		Factura actual = gestor.buscarFactura("AA11");
		assertSame(factura1, actual);
	}
	/*
	 * Test que comprueba el metodo buscarFactura(),
	 * con una factura inexistente con una factura en la lista,
	 * por lo que nos tiene que devolver null.
	 */
	@Test
	public void testBuscarFacturaInexistenteHabiendoFacturas() {
		gestor.listaFacturas.add(factura1);
		
		Factura actual = gestor.buscarFactura("BBBB");
		assertNull(actual);
	}
	/*
	 * Test que comprueba el metodo buscarFactura(),
	 * con una factura inexistente con varias facturas en la lista,
	 * por lo que nos tiene que devolver null.
	 */
	@Test
	public void testBuscarFacturaInexistenteHabiendoVariasFacturas() {
		gestor.listaFacturas.add(factura1);
		gestor.listaFacturas.add(factura2);
		
		Factura actual = gestor.buscarFactura("CCCC");
		assertNull(actual);
	}
	/*
	 * Test que comprueba el metodo buscarFactura(),
	 * con una factura existente con varias facturas en la lista,
	 * por lo que nos devolvera la factura existente.
	 */
	@Test
	public void testBuscarFacturaExistenteHabiendoVariasFacturas() {
		gestor.listaFacturas.add(factura1);
		gestor.listaFacturas.add(factura2);
		
		Factura actual = gestor.buscarFactura("BBBB");
		assertSame(factura2, actual);
	}
	/*
	 * Test que comprueba el metodo calcularPrecioTotal(),
	 * con una factura positiva, devolviendo su valor.
	 */
	@Test
	public void testCalcularPrecioTotal() {
		float actual = factura1.calcularPrecioTotal();
		float esperado = 7.5F;
		assertEquals(esperado, actual, 0.0);
	}
	/*
	 * Test que comprueba el metodo calcularPrecioTotal(),
	 * con una factura negativa, devolviendo su valor.
	 */
	@Test
	public void testCalcularPrecioTotalPrecioNegativo() {
		factura1.setPrecioUnidad(-2.5F);
		float actual = factura1.calcularPrecioTotal();
		float esperado = -7.5F;
		assertEquals(esperado, actual, 0.0);
	}
	/*
	 * Test que comprueba el metodo crearFactura(),
	 * con un codigo inexistente, por lo que la lista de
	 * facturas se incrementara en uno.
	 */
	@Test
	public void testCrearFacturaCodigoInexistente() {
		gestor.listaFacturas.add(factura1);
		gestor.crearFactura(factura2);
		
		int actual = gestor.listaFacturas.size();
		int esperado = 2;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comprueba el metodo crearFactura(),
	 * con un codigo existente, por lo que la lista de
	 * facturas mantiene su tama�o.
	 */
	@Test
	public void testCrearFacturaCodigoExistente() {
		gestor.listaFacturas.add(factura1);
		Cliente cliente3 = new Cliente ("Sergio", "1111111", LocalDate.now());
		Factura factura3 = new Factura("AA11", LocalDate.now(), "Videojuego", 2.5F, 3, cliente3); 
		gestor.crearFactura(factura3);
		
		int actual = gestor.listaFacturas.size();
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comprueba el metodo facturaMasCara(),
	 * con varias facturas en la lista, devolviendo
	 * como resultado la factura2 que es la mas cara en este caso.
	 */
	@Test
	public void testFacturaMasCaraConVariasFacturas() {
		gestor.listaFacturas.add(factura1);
		gestor.listaFacturas.add(factura2);
		
		Factura actual = gestor.facturaMasCara();
		assertEquals(factura2, actual);
	}
	/*
	 * Test que comprueba el metodo facturaMasCara(),
	 * sin facturas en la lista, teniendo que dar como
	 * resultado null para que pase el test.
	 */
	@Test
	public void testFacturaMasCaraSinFacturas() {
		Factura actual = gestor.facturaMasCara();
		assertNull(actual);
	}
	/*
	 * Test que comrpueba el metodo calcularFacturacionAnual(),
	 * con varias facturas, dando como resultado la suma de estas.
	 */
	@Test
	public void testCalcularFacturacionAnual() {
		gestor.listaFacturas.add(factura1);
		gestor.listaFacturas.add(factura2);
		
		float actual = gestor.calcularFacturacionAnual(2018);
		float esperado = 47.5F;
		assertEquals(esperado, actual, 0.0);
	}
	/*
	 * Test que comrpueba el metodo calcularFacturacionAnual(),
	 * sin facturas en la lsita, dando como resultado 0.
	 */
	@Test
	public void testCalcularFacturacionAnualSinFacturas() {
		float actual = gestor.calcularFacturacionAnual(2018);
		float esperado = 0;
		assertEquals(esperado, actual, 0.0);
	}
	/*
	 * Test que comprueba el metodo eliminarFactura(),
	 * borrando una factura existente.
	 * Como resultado, el tama�o del arraylist se incrementa en 1. 
	 */
	@Test
	public void testEliminarFacturaExistente() {
		gestor.listaFacturas.add(factura1);
		gestor.listaFacturas.add(factura2);
		gestor.eliminarFactura("AA11");
		
		int actual = gestor.listaFacturas.size();
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comprueba el metodo eliminarFactura(),
	 * borrando una factura inexistente.
	 * Como resultado, el tama�o del arraylist se mantiene. 
	 */
	@Test
	public void testEliminarFacturaInexistente() {
		gestor.listaFacturas.add(factura1);
		gestor.eliminarFactura("XXXX");
		
		int actual = gestor.listaFacturas.size();
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comrpueba el metodo asignarFacturaACliente(),
	 * con un cliente inexistente, por lo que a la factura que 
	 * se le asigna debera ser null el valor del cliente.
	 */
	@Test
	public void testAsignarFacturaAClienteInexistente() {
		gestor.listaClientes.add(cliente1);
		gestor.listaFacturas.add(factura1);
		gestor.asignarFacturaACliente("666666", "AA11");
		
		assertNull(factura1.getCliente());
	}
	/*
	 * Test que comrpueba el metodo asignarFacturaACliente(),
	 * con un cliente existente, por lo que a la factura que 
	 * se le asigna debera tener a ese cliente asignado.
	 */
	@Test
	public void testAsignarFacturaAClienteExistente() {
		gestor.listaClientes.add(cliente1);
		gestor.listaFacturas.add(factura1);
		gestor.asignarFacturaACliente("123456", "AA11");
		
		assertNotNull(factura1.getCliente());
	}
	/*
	 * Test que comprueba el metodo cantidadFacturaPorClientes(),
	 * en un cliente sin facturas, el resultado sera una lista de 0.
	 */
	@Test
	public void testCantidadFacturasPorClienteSinFacturas() {
		gestor.listaClientes.add(cliente1);
		
		int actual = gestor.cantidadFacturasPorCliente("123456");
		int esperado = 0;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comprueba el metodo cantidadFacturaPorClientes(),
	 * en un cliente con una factura, el resultado sera una lista de 1.
	 */
	@Test
	public void testCantidadFacturasPorClienteConUnaFactura() {
		gestor.listaClientes.add(cliente1);
		gestor.listaFacturas.add(factura1);
		gestor.asignarFacturaACliente("123456", "AA11");
		
		int actual = gestor.cantidadFacturasPorCliente("123456");
		int esperado = 1;
		assertEquals(esperado, actual);
	}
	/*
	 * Test que comprueba el metodo cantidadFacturaPorClientes(),
	 * en un cliente con varias factura, el resultado sera una lista de 2.
	 */
	@Test
	public void testCantidadFacturasPorClienteConVariasFacturas() {
		gestor.listaClientes.add(cliente1);
		gestor.listaFacturas.add(factura1);
		gestor.listaFacturas.add(factura2);
		gestor.asignarFacturaACliente("123456", "AA11");
		gestor.asignarFacturaACliente("123456", "BBBB");
		
		int actual = gestor.cantidadFacturasPorCliente("123456");
		int esperado = 2;
		assertEquals(esperado, actual);
	}
	
}

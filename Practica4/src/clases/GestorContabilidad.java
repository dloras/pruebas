package clases;

import java.util.ArrayList;

public class GestorContabilidad {

	public ArrayList<Factura> listaFacturas;
	public ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad() {
		listaFacturas = new ArrayList<Factura>();
		listaClientes = new ArrayList<Cliente>();
	}

	public Cliente buscarCliente(String dni) {
		for (Cliente cliente : listaClientes) {
			if (cliente.getDni().equals(dni)) {
				return cliente;
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigoFactura) {
		for (Factura factura : listaFacturas) {
			if (factura.getCodigoFactura().equals(codigoFactura)) {
				return factura;
			}
		}
		return null;
	}
	
	public void altaCliente(Cliente clienteAlta) {
		if (buscarCliente(clienteAlta.getDni()) == null) {
			listaClientes.add(clienteAlta);
		}
	}
	
	public void crearFactura(Factura facturaAlta) {
		if (buscarFactura(facturaAlta.getCodigoFactura()) == null) {
			listaFacturas.add(facturaAlta);
		}
	}
	
	public Cliente clienteMasAntiguo() {
		if (!listaClientes.isEmpty()) {
		Cliente clienteMayor = listaClientes.get(0);
			for (Cliente cliente : listaClientes) {
				if (cliente.getFechaAlta().isBefore(clienteMayor.getFechaAlta())) {
					clienteMayor = cliente;
				}
			}
			return clienteMayor;
		}
		return null;
	}
	
	public Factura facturaMasCara() {
		if (!listaFacturas.isEmpty()) {
			Factura facturaMayor = listaFacturas.get(0);
			for (Factura factura : listaFacturas) {
				if (factura.getPrecioUnidad() > facturaMayor.getPrecioUnidad()) {
					facturaMayor = factura;
				}
			}
			return facturaMayor;
		}
		return null;
	}
	
	public float calcularFacturacionAnual(int anno) {
		float facturacionAnual = 0;
		for (Factura factura : listaFacturas) {
			if (factura.getFecha().getYear() == anno) {
				facturacionAnual = facturacionAnual + factura.calcularPrecioTotal();
			}
		}
		return facturacionAnual;
	}
	
	public void asignarFacturaACliente(String dni, String codigoFactura) {
		buscarFactura(codigoFactura).setCliente(buscarCliente(dni));
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		int cantidadFacturas = 0;
		for (Factura factura : listaFacturas) {
			if (factura.getCliente().getDni().equals(dni)) {
				cantidadFacturas++;
			}
		}
		return cantidadFacturas;
	}
	
	public void eliminarFactura(String codigo) {
		for (Factura factura : listaFacturas) {
			if (factura.getCodigoFactura().equals(codigo)) {
				listaFacturas.remove(factura);
			}
		}
	}
	
	public void eliminarCliente(String dni) {
		for (Cliente cliente : listaClientes) {
			if (cliente.getDni().equals(dni)) {
				listaClientes.remove(cliente);
			}
		}
	}
		
}
